module gitlab.com/reederc42/gocover

go 1.18

require (
	github.com/spf13/cobra v1.5.0
	github.com/stretchr/testify v1.7.1
	golang.org/x/tools v0.1.12
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
