package names

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

// Counter calculates a cover variable name from provided string
func Counter(s string) string {
	return fmt.Sprintf("gocover_%s_counter", hash(s))
}

// Exit calculates an exit function name from provided string
func Exit(s string) string {
	return fmt.Sprintf("gocover_%s_exit", hash(s))
}

// Fatal calculates a fatal function name from provided string
func Fatal(s string) string {
	return fmt.Sprintf("gocover_%s_fatal", hash(s))
}

// Collect calculates a collect function name from provided string
func Collect(s string) string {
	return fmt.Sprintf("gocover_%s_collect", hash(s))
}

// Main calculates a main function name from provided string
func Main(s string) string {
	return fmt.Sprintf("gocover_%s_main", hash(s))
}

// Package calculates a package name from provided string
func Package(s string) string {
	return fmt.Sprintf("gocover_%s_pkg", hash(s))
}

func RegisterCounter(s string) string {
	return fmt.Sprintf("gocover_%s_registercounter", hash(s))
}

// Backup calculates backup file name; starts with _ to be ignored by Go
//  s should be module-relative filepath and f is file name
func Backup(s, f string) string {
	return fmt.Sprintf("_gocover_%s_backup_%s", hash(s), f)
}

func hash(s string) string {
	h := sha256.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}
