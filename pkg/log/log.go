// Package log implements standard logging for gocover
package log

import (
	"fmt"
	"os"
	"strings"
)

var Writer = os.Stderr

// Ln logs line
func Ln(a ...interface{}) {
	fmt.Fprintln(Writer, a...)
}

// Lnf logs formatted line, adding newline if not present
func Lnf(format string, a ...interface{}) {
	fmt.Fprintf(Writer, format, a...)
	if !strings.HasSuffix(format, "\n") {
		fmt.Fprint(Writer, "\n")
	}
}
