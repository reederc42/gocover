package source

import (
	"go/ast"
	"go/token"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/tools/go/ast/astutil"
)

// Module returns module name for directory dir, or empty string if dir is not a
// Go module
func Module(dir string) string {
	b, err := os.ReadFile(filepath.Join(dir, "go.mod"))
	if err != nil {
		return ""
	}
	return getModuleName(string(b))
}

func getModuleName(mod string) string {
	i := strings.Index(mod, "\n")
	if i < 0 {
		return ""
	}
	return strings.TrimPrefix(mod[:i], "module ")
}

// RemoveImport removes import i if f doesn't use it
func RemoveImport(i *ast.ImportSpec, fset *token.FileSet, f *ast.File) {
	realPath := i.Path.Value[1 : len(i.Path.Value)-1]
	if !astutil.UsesImport(f, realPath) {
		if i.Name == nil {
			astutil.DeleteImport(fset, f, realPath)
		} else {
			astutil.DeleteNamedImport(fset, f, i.Name.String(), realPath)
		}
	}
}

// ReplaceMainAndExits replaces all os.Exit idents and selectors, and main, with
// provided names. If coverMain and coverExit are empty, returns original ast
// unchanged. Returns if ast has been changed.
func ReplaceMainAndExits(root ast.Node, osImport, logImport *ast.ImportSpec,
	coverMain, coverExit, coverFatal, coverPkg string) (ast.Node, bool) {
	var changed bool
	var exitSelector string
	if osImport != nil {
		if osImport.Name != nil && osImport.Name.Name != "." {
			exitSelector = osImport.Name.Name
		} else {
			exitSelector = "os"
		}
	}
	var fatalSelector string
	if logImport != nil {
		if logImport.Name != nil && logImport.Name.Name != "." {
			fatalSelector = logImport.Name.Name
		} else {
			fatalSelector = "log"
		}
	}
	newTree := astutil.Apply(root, func(c *astutil.Cursor) bool {
		if coverMain != "" {
			if decl, ok := c.Node().(*ast.FuncDecl); ok &&
				decl.Name.String() == "main" {
				c.Replace(&ast.FuncDecl{
					Doc:  decl.Doc,
					Recv: decl.Recv,
					Name: &ast.Ident{
						Name: coverMain,
					},
					Type: decl.Type,
					Body: decl.Body,
				})
				changed = true
				return true
			}
		}
		if osImport != nil || logImport != nil {
			if sel, ok := c.Node().(*ast.SelectorExpr); ok {
				if osImport != nil && exitSelector != "" &&
					matchSelector(sel, exitSelector, "Exit") {
					replaceWithIdentOrSelector(c, coverPkg, coverExit)
					changed = true
					return true
				}
				if logImport != nil && fatalSelector != "" {
					if matchSelector(sel, fatalSelector, "Fatal") {
						replaceWithIdentOrSelector(c, coverPkg, coverFatal)
						changed = true
						return true
					}
					if matchSelector(sel, fatalSelector, "Fatalf") {
						replaceWithIdentOrSelector(c, coverPkg, coverFatal+"f")
						changed = true
						return true
					}
				}
			}
			if i, ok := c.Node().(*ast.Ident); ok {
				if osImport != nil && exitSelector == "" &&
					i.Name == "Exit" {
					replaceWithIdentOrSelector(c, coverPkg, coverExit)
					changed = true
					return true
				}
				if logImport != nil && fatalSelector == "" {
					if i.Name == "Fatal" {
						replaceWithIdentOrSelector(c, coverPkg, coverFatal)
						changed = true
						return true
					}
					if i.Name == "Fatalf" {
						replaceWithIdentOrSelector(c, coverPkg, coverFatal+"f")
						changed = true
						return true
					}
				}
			}
		}
		return true
	}, nil)
	return newTree, changed
}

// matchSelector matches selector s with p and n, i.e.: s == p.n
func matchSelector(s *ast.SelectorExpr, p, n string) bool {
	x, ok := s.X.(*ast.Ident)
	if !ok {
		return false
	}
	if x.Name != p {
		return false
	}
	return s.Sel.Name == n
}

// replaceWithIdentOrSelector replaces c with selector if p is not empty, or
// ident if p is empty
func replaceWithIdentOrSelector(c *astutil.Cursor, p, n string) {
	if p == "" {
		c.Replace(&ast.Ident{
			Name: n,
		})
	} else {
		c.Replace(&ast.SelectorExpr{
			X: &ast.Ident{
				Name: p,
			},
			Sel: &ast.Ident{
				Name: n,
			},
		})
	}
}
