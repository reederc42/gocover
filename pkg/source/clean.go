package source

import (
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/reederc42/gocover/pkg/log"
	"gitlab.com/reederc42/gocover/pkg/names"
	"gitlab.com/reederc42/gocover/pkg/shell"
)

var (
	pkgRE  = regexp.MustCompile(`^gocover_[0-9a-z]{64}_pkg.go$`)
	mainRE = regexp.MustCompile(`^gocover_[0-9a-z]{64}_main.go$`)
)

// CleanModule restores all backup files and deletes all package and main files
func CleanModule(directory, module string, verbose bool) error {
	coverPkg := names.Package(module)
	err := filepath.WalkDir(directory, func(path string, d fs.DirEntry,
		err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			if ShouldSkip(path, directory, coverPkg) {
				return filepath.SkipDir
			}
			return nil
		}
		// path is a backup file, restore it
		if o := Original(path, directory); o != "" {
			if err := shell.Exec("mv", path, o); err == nil && verbose {
				rp, _ := filepath.Rel(directory, o)
				if strings.HasPrefix(rp, "vendor/") {
					log.Ln(strings.TrimPrefix(rp, "vendor/"))
				} else {
					log.Ln(filepath.Join(module, rp))
				}
			}
		}
		if isPkg(path, directory) {
			// path is a generated package file, remove it
			if err := os.Remove(path); err == nil {
				rp, _ := filepath.Rel(directory, filepath.Dir(path))
				if verbose {
					log.Lnf("pkg: %s", filepath.Join(module, rp))
				}
			}
		} else if isMain(path, directory) {
			// path is a generated main file, remove it
			_ = os.Remove(path)
		}
		return nil
	})
	return err
}

// isPkg returns true if file is generated package file
func isPkg(path, modulePath string) bool {
	if !pkgRE.MatchString(filepath.Base(path)) {
		return false
	}
	d := filepath.Dir(path)
	r, err := filepath.Rel(modulePath, d)
	if err != nil {
		return false
	}
	return path == filepath.Join(d, names.Package(r)+".go")
}

// isMain returns true if file is generated main file
func isMain(path, modulePath string) bool {
	if !mainRE.MatchString(filepath.Base(path)) {
		return false
	}
	d := filepath.Dir(path)
	r, err := filepath.Rel(modulePath, d)
	if err != nil {
		return false
	}
	return path == filepath.Join(d, names.Main(r)+".go")
}
