package source

import (
	"go/ast"
	"go/parser"
	"go/token"
	"io/fs"
	"path/filepath"
	"strings"

	"gitlab.com/reederc42/gocover/pkg/log"
	"gitlab.com/reederc42/gocover/pkg/names"
)

// ShouldSkip returns true if directory should be skipped
// Don't skip root, even if it starts with _
// Skipped directories:
// 1. directories starting with "_"
// 2. directories starting with "."
// 3. testdata
// 4. directories ending with "_test"
// 5. cover package
func ShouldSkip(path, root, coverPkg string) bool {
	if path == root {
		return false
	}
	base := filepath.Base(path)
	return strings.HasPrefix(base, "_") ||
		strings.HasPrefix(base, ".") ||
		base == "testdata" ||
		strings.HasSuffix(base, "_test") ||
		base == coverPkg
}

// Discover walks moduleDir and returns all parsed packages and imports mapped
// by directory and file count (which will be equal to the number of counters)
func Discover(moduleDir, module string, includeImports,
	verbose bool) (map[string]map[string]*ast.Package,
	map[string]map[string]*ast.Package, int, error) {
	numFiles := 0
	dirs := make(map[string]map[string]*ast.Package)
	vendorDirs := make(map[string]map[string]*ast.Package)
	parserMode := parser.PackageClauseOnly
	coverPkg := names.Package(module)
	if includeImports {
		parserMode = parser.ImportsOnly
	}
	if verbose {
		log.Ln("discovering source files...")
	}
	err := filepath.WalkDir(moduleDir, func(path string, d fs.DirEntry,
		err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			return nil
		}
		if ShouldSkip(path, moduleDir, coverPkg) {
			return filepath.SkipDir
		}
		fset := token.NewFileSet()
		pkgs, err := parser.ParseDir(fset, path, nil, parserMode)
		if err != nil {
			return err
		}
		rpath, _ := filepath.Rel(moduleDir, path)
		isVendor := strings.HasPrefix(rpath, "vendor/")
		if len(pkgs) > 0 {
			// skip test files/pkgs
			for name, pkg := range pkgs {
				if strings.HasSuffix(name, "_test") {
					delete(pkgs, name)
				} else {
					for filePath := range pkg.Files {
						if strings.HasSuffix(filePath, "_test.go") ||
							// Skip backup and generated files; while they begin
							// with underscore, it seems sometimes the parser
							// collects them anyway
							Original(filePath, moduleDir) != "" ||
							isPkg(filePath, moduleDir) ||
							isMain(filePath, moduleDir) {
							delete(pkg.Files, filePath)
						} else if !isVendor {
							numFiles++
						}
					}
				}
			}
			if isVendor {
				vendorDirs[path] = pkgs
			} else {
				dirs[path] = pkgs
			}
			if verbose {
				log.Ln(rpath)
			}
		}
		return nil
	})
	if verbose && err == nil {
		log.Ln("discovered source files.")
	}
	return dirs, vendorDirs, numFiles, err
}
