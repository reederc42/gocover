package source

import (
	"path/filepath"
	"regexp"

	"gitlab.com/reederc42/gocover/pkg/names"
)

const backupREStr = `^_gocover_[0-9a-f]{64}_backup_(.*\.go)$`

var backupRE = regexp.MustCompile(backupREStr)

// Original returns the original path of backupPath, or empty string if
// backupPath is not a backup file.
func Original(backupPath, modulePath string) string {
	// check if base matches backup re
	base := filepath.Base(backupPath)
	m := backupRE.FindStringSubmatch(base)
	// m should hold entire match and original file name; if not this is not a
	// backup
	if len(m) != 2 {
		return ""
	}
	// check if hash matches expected hash
	// original file should be at dir + original base
	original := filepath.Join(filepath.Dir(backupPath), m[1])
	expected := Backup(original, modulePath)
	if expected == backupPath {
		return original
	}
	return ""
}

// Backup returns the path for a backup of the file at path
func Backup(path, modulePath string) string {
	rp, err := filepath.Rel(modulePath, path)
	if err != nil {
		return ""
	}
	return filepath.Join(filepath.Dir(path), names.Backup(rp,
		filepath.Base(path)))
}
