// Package v2 is documentation only.
//
// gocover v2 is intended to be a complete refactor for efficiency and
// correctness. One example is using go/parser for retrieving and replacing
// symbols. In the first iteration, a source file will be kept in memory, and in
// general used as a []byte, until all transformations are completed. v2 will
// also support parallelization.
//
// While the basic steps for each file are roughly the same, keeping source
// files in memory and supporting parallelization change certain details:
//
// 1. Cover source file - this is also the load step, and will return []byte
//
// 2. Backup source file (becomes simple mv instead of cp)
//
// 3. Parse source file - this is how we will find the exits and main functions
//that must be rewritten, and the file package. Information returned: 1. package
//name 2. location of main function 3. locations of all os.Exit and log.Fatal
//calls. 4. bool if imports have changed (if os or log packages are no longer
//used) 5. imports if they have changed
//
// 4. Rewrite source file - this will probably happen immediately after parsing.
//Sub-steps: 1. if imports have changed, remove existing imports and rewrite
//(maybe just comment them out?) 2. rewrite main function 3. rewrite exits 4.
//add counter initializer 5. write altered source file
//
// 5. return values from steps 3 and 4 will be sent to a channel to be processed
//out-of-band
//
// 3 & 4 are all handled by source.ReplaceMainAndExits
// Currently does not support log.Fatal
// Currently does not support source containg html escaped strings (they will be unescaped)
package v2

// TODOs/notes:
// add source discovery to v2
// potential main & exit function is calculated for every file
// since every counter adds itself to the global package, do we need to count them?
// not sure how to determine package for file, besides getting it from the token pos
// does parse dir return file nodes? yes it does!

// because parse dir returns file nodes, source discovery becomes:
// 1. WalkDir rooted at module
// 2. If file, skip
// 3. If dir, parse dir and store ast(s) - maybe just file name and imports and path if not nil

// source discovery now returns all found packages, files in them, and imports

// for each file in each package (excluding test files & packages), if os or log are imported, replace exits. if package is main, replace main
// for each package, write package collect, if main package write main file

// Names
// Package paths are calculated as (module) + ((package directory) - (module directory))
