package report

import "fmt"

type Labelset struct {
	Mode   Mode                        `json:"mode"`
	Blocks map[string]map[int][]string `json:"blocks,omitempty"`
}

// Profile returns a profile from a labelset and labels
func (l *Labelset) Profile(labels []string) Profile {
	p := Profile{
		Mode:   l.Mode,
		Blocks: make(map[string]int),
	}
	for k, v := range l.Blocks {
		m, i := maxCountForLabels(v, labels)
		if i {
			p.Blocks[k] = m
		}
	}
	return p
}

func maxCountForLabels(counts map[int][]string, labels []string) (int, bool) {
	max := -1
	for k, v := range counts {
		if !disjoint(v, labels) {
			max = maxInt(max, k)
		}
	}
	return max, max >= 0
}

// disjoint returns true if a and b are disjoint
func disjoint(a, b []string) bool {
	if len(a) == 0 || len(b) == 0 {
		return true
	}
	m := make(map[string]struct{})
	for _, v := range a {
		m[v] = struct{}{}
	}
	for _, v := range b {
		if _, ok := m[v]; ok {
			return false
		}
	}
	return true
}

// Labels returns all labels in a labelset
func (l *Labelset) Labels() []string {
	return nil
}

// NewLabelset creates a labelset from a profile and labels
func NewLabelset(p Profile, labels []string) Labelset {
	l := Labelset{
		Mode:   p.Mode,
		Blocks: make(map[string]map[int][]string),
	}
	for b, c := range p.Blocks {
		l.Blocks[b] = map[int][]string{c: labels}
	}
	return l
}

// MergeLabelsets merges two labelsets
func MergeLabelsets(a, b Labelset) (Labelset, error) {
	if a.Mode != b.Mode {
		return Labelset{}, fmt.Errorf("mismatched modes")
	}
	return Labelset{
		Mode:   a.Mode,
		Blocks: mergeLabeledBlocks(a.Blocks, b.Blocks),
	}, nil
}

func mergeLabeledBlocks(a,
	b map[string]map[int][]string) map[string]map[int][]string {
	for bBlock, bCountLabels := range b {
		aCountLabels, ok := a[bBlock]
		if !ok {
			a[bBlock] = bCountLabels
		} else {
			a[bBlock] = mergeCountLabels(aCountLabels, bCountLabels)
		}
	}
	return a
}

func mergeCountLabels(a, b map[int][]string) map[int][]string {
	for bCount, bLabels := range b {
		aLabels, ok := a[bCount]
		if !ok {
			a[bCount] = bLabels
		} else {
			a[bCount] = union(aLabels, bLabels)
		}
	}
	return a
}

// union returns union of a and b
func union(a, b []string) []string {
	m := make(map[string]struct{})
	for _, v := range a {
		m[v] = struct{}{}
	}
	for _, v := range b {
		m[v] = struct{}{}
	}
	o := make([]string, 0, len(m))
	for k := range m {
		o = append(o, k)
	}
	return o
}
