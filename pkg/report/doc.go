// Package report implements tools to produce and manage profiles. There are
// three operations: merge, label, and report, that operate on two data types:
// profiles and labelsets. Note report currently only supports the "set"
// coverage mode.
//
// Merge
//
// Merge two or more profiles or labelsets. This operation has different
// behavior depending on the input type. For profiles, merge matches each block
// of code (defined as module-qualified filename, start row & column, and end
// row & column) and stores the max count of the two profiles. Profiles do not
// have to be at all related: merging disjoint profiles would result in a
// profile that is simply each set of blocks appended.
// Merging labelsets requires preserving counts with each set of labels.
// For example:
// Given profile a:
// mode: set
// main.go:10.1,15.2 1
// and profile b:
// mode: set
// main.go:10.1,15.2 0
// and a has the labels [c,d]
// and b has the labels [c,e]
// then the resulting labelset would be:
// {
//   "mode": "set",
//   "blocks": {
//     "main.go:10.1,15.2": {
//       "0": ["c", "e"],
//       "1": ["c", "d"]
//     }
//   }
// }
// If a third profile, c, were:
// mode: set
// main.go:10.1,15.2 1
// with labels [d,f]
// the resulting labelset would be:
// {
//   "mode": "set",
//   "blocks": {
//     "main.go:10.1,15.2": {
//       "0": ["c", "e"],
//       "1": ["c", "d", "f"]
//     }
//   }
// }
//
// Label
//
// Label turns a profile into a labelset. See Merge for examples.
//
// Report
//
// Report generates a profile from a labelset, given labels. Any count with any
// matching label is included, and after the counts are merged like two
// profiles.
package report
