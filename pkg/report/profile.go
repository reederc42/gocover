package report

import (
	"bufio"
	"bytes"
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Profile struct {
	Mode   Mode
	Blocks map[string]int
}

var blockRE = regexp.MustCompile(`^([^:]+:\d+\.\d+,\d+\.\d+ \d+ )(\d+)$`)

// MergeProfiles merges a & b coverage profiles
func MergeProfiles(a, b Profile) (Profile, error) {
	if a.Mode != b.Mode {
		return Profile{}, fmt.Errorf("mismatched modes")
	}
	return Profile{
		Mode:   a.Mode,
		Blocks: mergeMapMax(a.Blocks, b.Blocks),
	}, nil
}

// NewProfile converts string profile to Profile type
func NewProfile(p string) (Profile, error) {
	var prof Profile
	prof.Blocks = make(map[string]int)
	m, err := GetMode(p)
	if err != nil {
		return Profile{}, err
	}
	prof.Mode = m

	scnr := bufio.NewScanner(bytes.NewBufferString(p))
	for scnr.Scan() {
		if !strings.HasPrefix(scnr.Text(), "mode: ") {
			match := blockRE.FindAllStringSubmatch(scnr.Text(), -1)
			if len(match) == 1 && len(match[0]) == 3 {
				i, err := strconv.Atoi(match[0][2])
				if err != nil {
					return Profile{}, err
				}
				prof.Blocks[match[0][1]] = i
			}
		}
	}

	return prof, nil
}

func (p Profile) String() string {
	l := make([]string, 0, len(p.Blocks))
	for k, _ := range p.Blocks {
		l = append(l, k)
	}
	sort.Strings(l)
	var b bytes.Buffer
	b.WriteString(fmt.Sprintf("mode: %s\n", p.Mode.String()))
	for _, k := range l {
		b.WriteString(fmt.Sprintf("%s%d\n", k, p.Blocks[k]))
	}
	return b.String()
}

// mergeMapMax merges maps a and b by max value of key
func mergeMapMax(a, b map[string]int) map[string]int {
	for k, v := range b {
		a[k] = maxInt(a[k], v)
	}
	return a
}

func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}
