package report

import (
	"encoding/json"
	"errors"
	"strings"
)

type Mode int

const (
	ModeFile Mode = iota
	ModeSet
)

// GetMode returns cover mode of fileContents; on error, mode is invalid
func GetMode(fileContents string) (Mode, error) {
	switch {
	case strings.HasPrefix(fileContents, "mode: set\n"):
		return ModeSet, nil
	case strings.HasPrefix(fileContents, "mode: "):
		return ModeFile, errors.New("unrecognized mode")
	default:
		return ModeFile, nil
	}
}

func (m Mode) String() string {
	switch m {
	case ModeSet:
		return "set"
	}
	return ""
}

func FromString(s string) Mode {
	switch s {
	case "set":
		return ModeSet
	}
	return ModeFile
}

func (m Mode) MarshalJSON() ([]byte, error) {
	return json.Marshal(m.String())
}

func (m *Mode) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*m = FromString(s)
	return nil
}
