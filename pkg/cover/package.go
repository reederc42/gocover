package cover

import (
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/reederc42/gocover/pkg/log"
	"gitlab.com/reederc42/gocover/pkg/names"
)

type Package struct {
	// package name
	Name string
	// absolute path of package
	Path string
	// module name
	Module string
	// absolute path of module
	ModulePath string
	// unhandled exit signals
	ExitSignals []string
}

func (p *Package) Cover(verbose bool) error {
	moduleQualifiedCoverPkg := filepath.Join(p.Module, names.Package(p.Module))
	rp, _ := filepath.Rel(p.ModulePath, p.Path)

	if p.Name == "main" {
		if err := writeMain(p.Path, rp, p.Module,
			moduleQualifiedCoverPkg, p.ExitSignals); err != nil {
			return err
		}
	}

	err := writePackageCollect(p.Path, rp, p.Name, p.Module,
		moduleQualifiedCoverPkg)
	if verbose && err == nil {
		log.Lnf("pkg: %s", filepath.Join(p.Module, rp))
	}
	return err
}

// writePackageCollect writes collect file for package at directory
func writePackageCollect(directory, moduleQualifiedPkg, pkg,
	module, moduleQualifiedCoverPkg string) error {
	tplData := struct {
		Package                 string
		ModuleQualifiedCoverPkg string
		ExitFunc                string
		CoverPkg                string
		RegisterCounterFunc     string
	}{
		Package:                 pkg,
		ModuleQualifiedCoverPkg: moduleQualifiedCoverPkg,
		ExitFunc:                names.Exit(moduleQualifiedPkg),
		CoverPkg:                names.Package(module),
		RegisterCounterFunc:     names.RegisterCounter(moduleQualifiedPkg),
	}
	filename := filepath.Join(directory,
		names.Package(moduleQualifiedPkg)+".go")
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	tpl, err := template.New("").Parse(pkgCollectTpl)
	if err != nil {
		return err
	}
	return tpl.Execute(f, tplData)
}

const pkgCollectTpl = `// Code generated by gocover DO NOT EDIT
package {{.Package}}

import "{{.ModuleQualifiedCoverPkg}}"

func {{.ExitFunc}}(code int) {
	{{.CoverPkg}}.Exit(code)
}

func {{.RegisterCounterFunc}}(file string, count []uint32, pos []uint32, stmts []uint16) {
	{{.CoverPkg}}.RegisterCounter(file, count, pos, stmts)
}
`

// writeMain writes main file for a main package
func writeMain(directory, moduleQualifiedPkg, module,
	moduleQualifiedCoverPkg string, exitSignals []string) error {
	tplData := struct {
		ModuleQualifiedCoverPkg string
		CoverPkg                string
		MainFunc                string
		ExitSignals             string
	}{
		ModuleQualifiedCoverPkg: moduleQualifiedCoverPkg,
		CoverPkg:                names.Package(module),
		MainFunc:                names.Main(moduleQualifiedPkg),
		ExitSignals:             strings.Join(exitSignals, ", "),
	}
	filename := filepath.Join(directory, names.Main(moduleQualifiedPkg)+".go")
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	tpl, err := template.New("").Parse(mainTpl)
	if err != nil {
		return err
	}
	return tpl.Execute(f, tplData)
}

const mainTpl = `// Code generated by gocover DO NOT EDIT
package main

import (
	{{- if .ExitSignals }}
	"os"
	"os/signal"
	"syscall"

	{{ end -}}
	"{{.ModuleQualifiedCoverPkg}}"
)

func main() {
	defer {{.CoverPkg}}.Collect()
	{{ if .ExitSignals -}}
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, {{.ExitSignals}})
		s :=<-c
		e := 1
		if us, ok := s.(syscall.Signal); ok {
			e = int(us)
		}
		{{.CoverPkg}}.Collect()
		os.Exit(e)
	}()
	{{ end -}}
	{{.MainFunc}}()
}
`
