package cover

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"os"
	"path/filepath"

	"golang.org/x/tools/go/ast/astutil"

	"gitlab.com/reederc42/gocover/pkg/log"
	"gitlab.com/reederc42/gocover/pkg/shell"
	"gitlab.com/reederc42/gocover/pkg/source"
)

type VendorFile struct {
	Imports []*ast.ImportSpec
	// absolute path of file
	Path string
	// import path of cover package
	CoverPackage string
	// absolute path of module
	ModulePath string
	// absolute path of vendor dir
	VendorPath string
}

func (v *VendorFile) Cover(verbose bool) error {
	rpath, _ := filepath.Rel(v.VendorPath, v.Path)
	backup := source.Backup(v.Path, v.ModulePath)
	if _, err := os.Stat(backup); err == nil {
		if verbose {
			log.Ln(rpath)
		}
		return nil
	}
	var osImport, logImport *ast.ImportSpec
	for _, i := range v.Imports {
		switch i.Path.Value {
		case "\"os\"":
			osImport = i
		case "\"log\"":
			logImport = i
		}
	}
	if osImport != nil || logImport != nil {
		coverExit := "Exit"
		coverFatal := "Fatal"
		fset := token.NewFileSet()
		fileTree, err := parser.ParseFile(fset, v.Path, nil,
			parser.ParseComments)
		if err != nil {
			return err
		}
		newTree, changed := source.ReplaceMainAndExits(fileTree, osImport,
			logImport, "", coverExit, coverFatal, filepath.Base(v.CoverPackage))
		if f, ok := newTree.(*ast.File); ok {
			fileTree = f
		} else {
			return fmt.Errorf("could not assert node as file")
		}
		// if tree has changed, backup file and write new source
		if changed {
			// backup
			if err := shell.Exec("mv", v.Path, backup); err != nil {
				return err
			}
			// add cover package
			astutil.AddImport(fset, fileTree, v.CoverPackage)
			// check if os and log are still used
			if osImport != nil {
				source.RemoveImport(osImport, fset, fileTree)
			}
			if logImport != nil {
				source.RemoveImport(logImport, fset, fileTree)
			}
			// rewrite
			f, err := os.Create(v.Path)
			if err != nil {
				return err
			}
			defer f.Close()
			if err := printer.Fprint(f, fset, fileTree); err != nil {
				return err
			}
			if verbose {
				log.Ln(rpath)
			}
		}
	}
	return nil
}
