package cover

import (
	"go/ast"
	"path/filepath"

	"gitlab.com/reederc42/gocover/pkg/log"
	"gitlab.com/reederc42/gocover/pkg/names"
)

type Coverable interface {
	Cover(verbose bool) error
}

type CoverModuleArgs struct {
	GoCmd           string
	Module          string
	ModulePath      string
	CoverProfile    string
	CoverProfileEnv string
	SourceDirs      map[string]map[string]*ast.Package
	VendorDirs      map[string]map[string]*ast.Package
	NumCounters     int
	ExitSignals     []string
}

// CoverModule rewrites a Go module with code coverage
func CoverModule(args CoverModuleArgs, verbose bool) error {
	if verbose {
		log.Ln("covering source files...")
	}
	m := Module{
		Name:            args.Module,
		Path:            args.ModulePath,
		NumCounters:     args.NumCounters,
		CoverProfile:    args.CoverProfile,
		CoverProfileEnv: args.CoverProfileEnv,
	}
	if err := m.Cover(verbose); err != nil {
		return err
	}

	for path, pkgs := range args.SourceDirs {
		for pkgName, pkg := range pkgs {
			rpath, err := filepath.Rel(args.ModulePath, path)
			if err != nil {
				return err
			}

			p := Package{
				Name:        pkgName,
				Path:        path,
				Module:      args.Module,
				ModulePath:  args.ModulePath,
				ExitSignals: args.ExitSignals,
			}
			if err := p.Cover(verbose); err != nil {
				return err
			}

			for filePath, file := range pkg.Files {
				f := File{
					Imports:     file.Imports,
					Path:        filePath,
					Package:     rpath,
					PackageName: pkgName,
					Module:      args.Module,
					ModulePath:  args.ModulePath,
					GoCmd:       args.GoCmd,
				}
				if err := f.Cover(verbose); err != nil {
					return err
				}
			}
		}
	}
	if verbose {
		log.Ln("covered source files.")
	}

	if verbose {
		log.Ln("replacing vendor files...")
	}
	moduleQualifiedCoverPkg := filepath.Join(args.Module,
		names.Package(args.Module))
	vendorPath := filepath.Join(args.ModulePath, "vendor")
	for _, pkgs := range args.VendorDirs {
		for _, pkg := range pkgs {
			for p, f := range pkg.Files {
				v := VendorFile{
					Imports:      f.Imports,
					Path:         p,
					CoverPackage: moduleQualifiedCoverPkg,
					ModulePath:   args.ModulePath,
					VendorPath:   vendorPath,
				}
				if err := v.Cover(verbose); err != nil {
					return err
				}
			}
		}
	}
	if verbose {
		log.Ln("replaced vendor exits.")
	}

	return nil
}
