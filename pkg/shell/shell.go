// build tag added to force error on windows
//go:build !windows
// +build !windows

// Package cmd has helper functions for exec'ing processes
package shell

import (
	"bytes"
	"os"
	"os/exec"
)

func LookPath(cmd string) (string, error) {
	return exec.LookPath(cmd)
}

// Exec runs cmd and args in cmd, forwarding to stdout and stderr; returns if cmd exited successfully
func Exec(cmd string, args ...string) error {
	c := exec.Command(cmd, args...)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	return c.Run()
}

// ExecStdout runs cmd and args, forwarding stderr; returns stdout as string
func ExecStdout(cmd string, args ...string) (string, error) {
	var o bytes.Buffer
	c := exec.Command(cmd, args...)
	c.Stdout = &o
	c.Stderr = os.Stderr
	err := c.Run()
	return o.String(), err
}

// ExecStdoutStderr runs cmd and args; returns stdout and stderr as string
func ExecStdoutStderr(cmd string, args ...string) (string, string, error) {
	var o, e bytes.Buffer
	c := exec.Command(cmd, args...)
	c.Stdout = &o
	c.Stderr = &e
	err := c.Run()
	return o.String(), e.String(), err
}
