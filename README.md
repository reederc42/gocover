# gocover

Coverage analysis for go programs

gocover provides a nearly "drop-in" binary replacement for analyzing code
coverage in real-world situations, or with external testing frameworks. A
"drop-in" replacement is intended, but for now users must call `gocover cover`
on the source before calling `go build`.

Very special thanks to the Go team: much of this is lifted from and inspired by
`cmd/internal/testing`

## Operations

Describes gocover operations

### Cover

Usage: `gocover cover --cover-profile {file} | --cover-profile-env 
  {environment variable}`

Options:

`--cover-profile string`

default: `coverage-*.out`

    Sets the name of the generated report. If the name contains the wildcard
    '*', every instance creates a unique file to record coverage.

`--cover-profile-env string`

    Use the provided environment variable as the name of the generated report.
    If the name contains the wildcard '*', every instance creates a unique file
    to record coverage. Overrides cover-profile option.

`--no-exit-signals | -n`

    By default, gocover adds handlers for SIGTERM, SIGINT, and the operating
    system dependent 'os.Interrupt' signal. If the program being covered handles
    those signals already, this option should be set to avoid premature exits.

`--go`

    Specifies Go command to use to cover source code. Allows gocover to work
    with multiple versions of Go.

Annotates and builds source files with coverage report. Under the hood, it
calls `go tool cover` on each go file in the source tree, ensuring the produced
variable names are unique. It then rewrites all program terminations to record
the coverage report.

There are 5 ways to terminate a program in Go:

1. Panic (without recover)
2. os.Exit
3. Return in main
4. Main ends
5. Signal

gocover handles cases 1, 3, and 4 by wrapping all main functions. The wrapper
uses a deferred function to collect & write the coverage report. Deferred
functions are guaranteed to be called when the function returns normally, or on
panics. Case 2 is handled by rewriting all os.Exit calls with another function
that collects & writes the coverage report before calling os.Exit itself. Case 5
is handled by adding signal handlers that collect & write the coverage report
before exiting.

After the source has been annotated and rewritten, `go build` is called with
any and all provided build flags sent unchanged.

#### Collect & Record

`gocover` uses `go tool cover` to add a unique variable in each file, of the
form `gocover_%s_counter` where %s is the hash of the file's name and path,
relative to module root.

`gocover` produces a function inside a unique package that collects and records
all cover variables. It does this by calling, for each cover variable, a print
function that writes to the configured report file.

### Merge

Usage: `gocover merge <files...>`

Merges two or more coverage files of the format produced by `go test -cover` to
maximize reported coverge: i.e., if a block is covered in any supplied coverage
report, the merged report will mark the block as covered. 

Note this format is not a standard, and may change in the future, potentially
breaking gocover.

### Label

Usage: `gocover label --exsting-labels <file> --label <label> <files...>`

Options:

`--existing-labels | -f string`
 
    Existing labelset to merge provided reports and label into

`--label | -l string`
 
    required: true

    Label to assign to covered blocks or files in labelset

Inverts coverage report, labeling each covered block (or file) with provided
label and merging the labeled blocks with any existing labels. If multiple
reports are provided, they are merged as if `merge` was called first.

## Limitations

- `go tool cover` provides 3 modes of coverage. gocover currently only supports
  "set"

- searching for go module is currently limited to linux
  systems (with root at /)
