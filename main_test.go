package main_test

import (
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/reederc42/gocover/pkg/shell"
)

const downloadFmt = "golang.org/dl/%s@latest"

var testModules = []struct {
	// name must be unique
	name      string
	goVersion string
	repoURL   string
	builds    [][]string
}{
	{
		name:      "wiki",
		goVersion: "go1.19",
		repoURL:   "https://gitlab.com/reederc42/wiki.git",
		builds: [][]string{
			{"."},
		},
	},
	{
		name:      "trident",
		goVersion: "go1.18",
		repoURL:   "https://github.com/NetApp/trident.git",
		builds: [][]string{
			{"."},
		},
	},
}

func TestBuildModule(t *testing.T) {
	// for each module:
	// 1. check go is available
	// 2. check git is available
	// 3. clone module
	// 4. cover module
	// 5. build module
	// 6. clean module
	// 7. build module
	// 8. remove module
	_, err := shell.LookPath("git")
	assert.NoError(t, err)
	cd, _ := os.Getwd()
	for _, module := range testModules {
		t.Run(module.name, func(t *testing.T) {
			var (
				goCmd     string
				moduleDir string
				stderr    string
				err       error
			)
			moduleDir = "./testdata/" + module.name
			defer os.RemoveAll(moduleDir)
			defer os.Chdir(cd)
			t.Run("ensure go", func(t *testing.T) {
				goCmd = ensureGoVersion(t, module.goVersion)
			})
			t.Run("git clone", func(t *testing.T) {
				_, stderr, err = shell.ExecStdoutStderr(
					"git",
					"clone",
					module.repoURL,
					moduleDir,
				)
				assert.NoError(t, err)
			})
			t.Run("cover module", func(t *testing.T) {
				_, _, err = shell.ExecStdoutStderr(
					"ls",
					moduleDir+"/vendor",
				)
				if err != nil {
					os.Chdir(moduleDir)
					_, _, err = shell.ExecStdoutStderr(
						goCmd,
						"mod",
						"vendor",
					)
					assert.NoError(t, err)
					os.Chdir(cd)
				}
				_, stderr, err = shell.ExecStdoutStderr(
					goCmd,
					"run",
					".",
					"cover",
					fmt.Sprintf("--go=%s", goCmd),
					moduleDir,
				)
				assert.NoError(t, err, stderr)
			})
			t.Run("build covered module", func(t *testing.T) {
				os.Chdir(moduleDir)
				for _, build := range module.builds {
					_, stderr, err = shell.ExecStdoutStderr(
						goCmd,
						append([]string{"build"}, build...)...,
					)
					assert.NoError(t, err, stderr)
				}
			})
			t.Run("clean module", func(t *testing.T) {
				os.Chdir(cd)
				_, stderr, err = shell.ExecStdoutStderr(
					goCmd,
					"run",
					".",
					"clean",
					moduleDir,
				)
				assert.NoError(t, err, stderr)
			})
			t.Run("build clean module", func(t *testing.T) {
				os.Chdir(moduleDir)
				for _, build := range module.builds {
					_, stderr, err = shell.ExecStdoutStderr(
						goCmd,
						append([]string{"build"}, build...)...,
					)
					assert.NoError(t, err, stderr)
				}
			})
		})
	}
}

// ensureGoVersion ensures specified version is available, and returns the
// binary path. If version is not available, it is installed.
func ensureGoVersion(t *testing.T, version string) string {
	t.Helper()
	cmdVersion, err := shell.ExecStdout("go", "version")
	// Go command should be available
	assert.NoError(t, err)
	if strings.Contains(cmdVersion, version) {
		return "go"
	}
	// default version doesn't match version, check if available
	// technically version could be installed and not downloaded; we're ignoring
	// that case
	path, err := shell.LookPath(version)
	if err == nil {
		return path
	}
	// version not available, download
	_, _, err = shell.ExecStdoutStderr(
		"go",
		"install",
		fmt.Sprintf(downloadFmt, version),
	)
	assert.NoError(t, err)
	_, _, err = shell.ExecStdoutStderr(
		version,
		"download",
	)
	assert.NoError(t, err)
	return version
}
