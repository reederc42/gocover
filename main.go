package main

import (
	"os"

	"gitlab.com/reederc42/gocover/cmd"
)

func main() {
	if c := cmd.Execute(); c != 0 {
		os.Exit(c)
	}
}
