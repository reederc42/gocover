package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var analyzeCmd = &cobra.Command{
	Use:   "analyze",
	Short: "Analyzes coverage report",
	Long: `Analyzes coverage report

Prints coverage percentage. Verbose prints per-file coverage, ordered by most covered.`,
	Args: cobra.ExactArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		return fmt.Errorf("unimplemented")
	},
}

func init() {
	rootCmd.AddCommand(analyzeCmd)
}
