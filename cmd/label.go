package cmd

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/reederc42/gocover/pkg/report"
)

var (
	existingLabels string
	labels         []string
	output         string
)

var labelCmd = &cobra.Command{
	Use:   "label",
	Short: "Label coverage profiles",
	Long: `Label coverage profiles

Labels coverage profiles with provided labels, producing a labelset. Profiles
are merged like the "merge" command before labels are added. If an existing
labelset is provided, the labeled profile is merged into the existing labelset.`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		var profile report.Profile
		{
			b, err := ioutil.ReadFile(args[0])
			if err != nil {
				return err
			}
			p, err := report.NewProfile(string(b))
			if err != nil {
				return err
			}
			profile = p
			if verbose {
				logBasepath(args[0])
			}
		}

		for i := 1; i < len(args); i++ {
			c, err := ioutil.ReadFile(args[i])
			if err != nil {
				return err
			}
			p, err := report.NewProfile(string(c))
			if err != nil {
				return err
			}
			profile, err = report.MergeProfiles(profile, p)
			if err != nil {
				return err
			}
			if verbose {
				logBasepath(args[i])
			}
		}

		l := report.NewLabelset(profile, labels)

		if existingLabels != "" {
			f, err := os.Open(existingLabels)
			if err != nil {
				return err
			}
			defer f.Close()
			var existingLabels report.Labelset
			if err := json.NewDecoder(f).Decode(&existingLabels); err != nil {
				return err
			}
			m, err := report.MergeLabelsets(l, existingLabels)
			if err != nil {
				return err
			}
			l = m
		}

		out := os.Stdout
		if output != "" {
			f, err := os.Create(output)
			if err != nil {
				return err
			}
			defer f.Close()
			out = f
		}

		return json.NewEncoder(out).Encode(l)
	},
}

func init() {
	labelCmd.Flags().StringVarP(&existingLabels, "existing-labels", "f", "",
		"existing labelset")
	labelCmd.PersistentFlags().StringSliceVarP(&labels, "labels", "l", nil,
		"labels to assign to coverage report")
	labelCmd.Flags().StringVarP(&output, "output", "o", "",
		"Output file; allows rewriting existing labelset (default stdout)")
	_ = labelCmd.MarkFlagRequired("labels")

	rootCmd.AddCommand(labelCmd)
}
