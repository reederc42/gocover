package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"

	"gitlab.com/reederc42/gocover/pkg/report"
)

var mergeCmd = &cobra.Command{
	Use:   "merge",
	Short: "Merges coverage profiles",
	Long: `Merges multiple coverage profiles

Each block is merged by setting use count to the greater of the counts.`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		var profile report.Profile
		{
			b, err := ioutil.ReadFile(args[0])
			if err != nil {
				return err
			}
			p, err := report.NewProfile(string(b))
			if err != nil {
				return err
			}
			profile = p
			if verbose {
				logBasepath(args[0])
			}
		}

		for i := 1; i < len(args); i++ {
			c, err := ioutil.ReadFile(args[i])
			if err != nil {
				return err
			}
			p, err := report.NewProfile(string(c))
			if err != nil {
				return err
			}
			profile, err = report.MergeProfiles(profile, p)
			if err != nil {
				return err
			}
			if verbose {
				logBasepath(args[i])
			}
		}

		fmt.Print(profile.String())
		return nil
	},
}

func init() {
	rootCmd.AddCommand(mergeCmd)
}

func logBasepath(path string) {
	fmt.Fprintf(os.Stderr, "%s\n", filepath.Base(path))
}
