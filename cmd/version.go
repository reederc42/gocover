package cmd

import (
	"encoding/json"
	"os"
	"runtime"

	"github.com/spf13/cobra"
)

const version = "v0.0.2"

var shortVersion bool

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Prints gocover version",
	Run: func(_ *cobra.Command, _ []string) {
		var versionData = struct {
			GoCover        string `json:"gocover"`
			RuntimeVersion string `json:"runtime,omitempty"`
		}{
			GoCover: version,
		}
		enc := json.NewEncoder(os.Stdout)
		if shortVersion {
			enc.Encode(&versionData)
			return
		}
		versionData.RuntimeVersion = runtime.Version()
		if verbose {
			enc.SetIndent("", "  ")
		}
		enc.Encode(&versionData)
	},
}

func init() {
	versionCmd.Flags().BoolVarP(&shortVersion, "short", "s", false,
		"Print gocover version only (overrides verbose)")

	rootCmd.AddCommand(versionCmd)
}
