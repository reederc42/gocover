package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/reederc42/gocover/pkg/cover"
	"gitlab.com/reederc42/gocover/pkg/shell"
	"gitlab.com/reederc42/gocover/pkg/source"
)

var (
	coverProfile    string
	coverProfileEnv string
	goCmd           string
	noExitSignals   bool
)

// signals commonly used with containers
var defaultExitSignals = []string{
	"syscall.SIGINT",
	"syscall.SIGTERM",
	"os.Interrupt",
}

var coverCmd = &cobra.Command{
	Use: "cover [module]",
	Short: "Adds coverage to Go module. By default traps SIGINT and SIGTERM " +
		"(also \nos.Interrupt for Windows compatibility)",
	Args: cobra.ExactArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		exactGoCmd, err := shell.LookPath(goCmd)
		if err != nil {
			return err
		}
		if err := checkGoVersion(exactGoCmd); err != nil {
			return err
		}

		module := source.Module(args[0])
		if module == "" {
			return fmt.Errorf("not a go module")
		}
		moduleDir, _ := filepath.Abs(args[0])
		if info, err := os.Stat(filepath.Join(moduleDir,
			"vendor")); err != nil || !info.IsDir() {
			return fmt.Errorf("vendor does not exist or is not dir")
		}
		dirs, vendorDirs, numCounters, err := source.Discover(moduleDir, module,
			true, verbose)
		if err != nil {
			return err
		}
		exitSignals := defaultExitSignals
		if noExitSignals {
			exitSignals = nil
		}
		coverModuleArgs := cover.CoverModuleArgs{
			GoCmd:           goCmd,
			Module:          module,
			ModulePath:      moduleDir,
			CoverProfile:    coverProfile,
			CoverProfileEnv: coverProfileEnv,
			SourceDirs:      dirs,
			VendorDirs:      vendorDirs,
			NumCounters:     numCounters,
			ExitSignals:     exitSignals,
		}
		return cover.CoverModule(coverModuleArgs, verbose)
	},
}

// checkGoVersion checks if version of provided goCmd matches runtime
func checkGoVersion(goCmd string) error {
	re := regexp.MustCompile(`^go\d+\.\d+(?:\.\d+)?$`)
	cmdVersion, err := shell.ExecStdout(goCmd, "version")
	if err != nil {
		return err
	}
	for _, v := range strings.Split(cmdVersion, " ") {
		if re.MatchString(v) {
			rv := runtime.Version()
			if v != rv {
				return fmt.Errorf("runtime %s does not match configured command %s",
					rv, v)
			}
			return nil
		}
	}
	return fmt.Errorf("could not find go version")
}

func init() {
	coverCmd.Flags().StringVar(&coverProfile, "cover-profile", "coverage-*.out",
		"coverage-profile")
	coverCmd.Flags().StringVar(&coverProfileEnv, "cover-profile-env", "",
		"env variable defining coverage profile (overrides cover-profile)")
	coverCmd.Flags().StringVar(&goCmd, "go", "go", "Go command")
	coverCmd.Flags().BoolVarP(&noExitSignals, "no-exit-signals", "n", false,
		"do not trap SIGINT and SIGTERM")

	rootCmd.AddCommand(coverCmd)
}
