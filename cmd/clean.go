package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"

	"gitlab.com/reederc42/gocover/pkg/log"
	"gitlab.com/reederc42/gocover/pkg/names"
	"gitlab.com/reederc42/gocover/pkg/source"
)

var cleanCmd = &cobra.Command{
	Use:   "clean [module]",
	Short: "Cleans a covered Go module to return it to uncovered state",
	Args:  cobra.ExactArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		module := source.Module(args[0])
		if module == "" {
			return fmt.Errorf("not a go module")
		}
		moduleDir, _ := filepath.Abs(args[0])
		if verbose {
			log.Ln("cleaning module...")
		}
		_ = os.RemoveAll(filepath.Join(moduleDir, names.Package(module)))
		if verbose {
			log.Lnf("mod: %s", module)
		}
		if err := source.CleanModule(moduleDir, module, verbose); err != nil {
			return err
		}
		if verbose {
			log.Ln("cleaned module.")
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(cleanCmd)
}
