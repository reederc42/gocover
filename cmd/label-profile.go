package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/reederc42/gocover/pkg/report"
)

var labelProfileCmd = &cobra.Command{
	Use:   "profile",
	Short: "Gets coverage profile from labelsets",
	Args:  cobra.MinimumNArgs(1),
	RunE: func(_ *cobra.Command, args []string) error {
		var labelset report.Labelset
		{
			f, err := os.Open(args[0])
			if err != nil {
				return err
			}
			defer f.Close()
			var l report.Labelset
			if err := json.NewDecoder(f).Decode(&l); err != nil {
				return err
			}
			labelset = l
			if verbose {
				logBasepath(args[0])
			}
		}

		for i := 1; i < len(args); i++ {
			f, err := os.Open(args[0])
			if err != nil {
				return err
			}
			defer f.Close()
			var l report.Labelset
			if err := json.NewDecoder(f).Decode(&l); err != nil {
				return err
			}
			labelset, err = report.MergeLabelsets(labelset, l)
			if err != nil {
				return err
			}
			if verbose {
				logBasepath(args[i])
			}
		}

		fmt.Print(labelset.Profile(labels).String())
		return nil
	},
}

func init() {
	_ = labelProfileCmd.MarkPersistentFlagRequired("labels")
	labelCmd.AddCommand(labelProfileCmd)
}
